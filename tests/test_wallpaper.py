"""
Test cases for the wallpaper module.

Author: Benedikt Vollmerhaus
License: MIT
"""

import pathlib

from blurwal import paths, wallpaper


def test_changed_externally_returns_false_when_transition():
    assert not wallpaper.changed_externally(paths.CACHE_DIR / 'frame-4.jpg')


def test_changed_externally_returns_false_when_still_original(mocker,
                                                              shared_datadir):
    mocker.patch('blurwal.paths.get_original',
                 return_value=shared_datadir / 'original.jpg')
    assert not wallpaper.changed_externally(shared_datadir / 'original.jpg')


def test_changed_externally_returns_true_when_not_original(mocker,
                                                           shared_datadir):
    mocker.patch('blurwal.paths.get_original',
                 return_value=shared_datadir / 'original.jpg')
    assert wallpaper.changed_externally(shared_datadir / 'wallpaper.jpg')


def test_is_transition():
    assert wallpaper.is_transition(paths.CACHE_DIR / 'frame-4.jpg')


def test_is_transition_returns_false_when_filename_not_frame():
    assert not wallpaper.is_transition(paths.CACHE_DIR / 'not-a-frame.jpg')


def test_is_transition_returns_false_when_not_in_cache_dir():
    assert not wallpaper.is_transition(pathlib.Path.home() / 'frame-4.jpg')
