"""
Test cases for the feh backend.

Author: Benedikt Vollmerhaus
License: MIT
"""

import pathlib

import pytest

from blurwal.backends import feh

#: The target to patch the FEHBG_FILE class constant at
FEHBG_FILE_CONSTANT = 'blurwal.backends.feh.FehBackend.FEHBG_FILE'


def test_change_to_runs_feh(mocker, feh_backend):
    wallpaper_path = pathlib.Path('/home/user/images/wallpaper.jpg')
    run_mock = mocker.patch('subprocess.run')
    feh_backend.change_to(wallpaper_path)
    run_mock.assert_called_once_with(
        ['feh', '--bg-fill', str(wallpaper_path)])


def test_get_current(mocker, datadir, feh_backend):
    mocker.patch(FEHBG_FILE_CONSTANT, datadir / '.fehbg')
    assert (feh_backend.get_current() ==
            pathlib.Path('/home/user/images/wallpaper.jpg'))


def test_get_current_with_fully_quoted_fehbg(mocker, datadir, feh_backend):
    mocker.patch(FEHBG_FILE_CONSTANT, datadir / '.fehbg_fully_quoted')
    assert (feh_backend.get_current() ==
            pathlib.Path('/home/user/images/wallpaper.jpg'))


def test_get_current_returns_none_when_invalid(mocker, datadir, feh_backend):
    mocker.patch(FEHBG_FILE_CONSTANT, datadir / '.fehbg_invalid')
    assert feh_backend.get_current() is None


def test_get_current_exits_when_fehbg_missing(mocker, datadir, feh_backend):
    mocker.patch(FEHBG_FILE_CONSTANT, datadir / 'non-existent/.fehbg')
    with pytest.raises(SystemExit):
        feh_backend.get_current()


def test_restore_original(mocker, datadir, feh_backend):
    mocker.patch('blurwal.paths.ORIGINAL_PATH', datadir / 'original-path')
    expected_original_path = pathlib.Path('/home/user/images/original.jpg')

    run_mock = mocker.patch('subprocess.run')
    feh_backend.restore_original()
    run_mock.assert_called_once_with(
        ['feh', '--bg-fill', str(expected_original_path)])


def test_restore_original_exits_when_missing(mocker, datadir, feh_backend):
    mocker.patch('blurwal.paths.ORIGINAL_PATH', datadir / 'non-existent')
    with pytest.raises(SystemExit):
        feh_backend.restore_original()


@pytest.fixture(scope='module')
def feh_backend():
    return feh.FehBackend()
