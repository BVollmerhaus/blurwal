"""
Test cases for the transition thread.

Author: Benedikt Vollmerhaus
License: MIT
"""

import time
from unittest.mock import create_autospec

import pytest

from blurwal.backends import base
from blurwal.transition import Transition


def change_to_with_delay(_path: str):
    """
    Mock :code:`change_to` with sleep to simulate the backend operating.
    """
    time.sleep(1)


def test_run_blur(backend_mock):
    thread = Transition(1, 9, backend_mock)
    thread.start()
    thread.join()  # Wait for thread completion
    assert backend_mock.change_to.call_count == 8


def test_run_unblur(backend_mock):
    thread = Transition(8, 2, backend_mock)
    thread.start()
    thread.join()  # Wait for thread completion
    assert backend_mock.change_to.call_count == 6


def test_stop_cancels_transition(backend_mock):
    backend_mock.change_to.side_effect = change_to_with_delay

    thread = Transition(0, 10, backend_mock)
    thread.start()
    thread.stop()
    assert thread.is_stopped()
    assert backend_mock.change_to.call_count == 1


@pytest.fixture
def backend_mock():
    return create_autospec(base.Backend)
