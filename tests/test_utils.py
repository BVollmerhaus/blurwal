"""
Test cases for the utility module.

Author: Benedikt Vollmerhaus
License: MIT
"""

from pytest import approx

from blurwal import utils


def test_map_range_with_positive():
    assert utils.map_range(0.5, (0, 1), (0, 10)) == 5
    assert utils.map_range(2, (0, 10), (0, 1)) == 0.2

    assert utils.map_range(128, (0, 255), (0, 1)) == approx(0.5019607)
    assert utils.map_range(6, (10, 48), (5, 9)) == approx(4.578947)


def test_map_range_with_negative():
    assert utils.map_range(0.5, (0, 1), (0, -10)) == -5
    assert utils.map_range(-2, (0, -10), (0, 1)) == 0.2
    assert utils.map_range(-8, (0, -10), (0, -1)) == -0.8


def test_map_range_with_out_of_range_values():
    assert utils.map_range(6, (0, 1), (0, 10)) == 60
    assert utils.map_range(20.5, (10, 20), (0, 10)) == 10.5

    assert utils.map_range(5.2, (10, 20), (0, 10)) == -4.8
    assert utils.map_range(-4, (0, 1), (1, 2)) == -3


def test_show_notification(mocker):
    run_mock = mocker.patch('subprocess.run')
    utils.show_notification('A Title', 'Hello, World')
    run_mock.assert_called_once_with(
        ['notify-send', 'A Title', 'Hello, World'])


def test_show_notification_libnotify_is_optional(mocker):
    mocker.patch('subprocess.run', side_effect=FileNotFoundError)
    utils.show_notification('A Title', 'Hello, World')


def test_print_heading(capsys):
    utils.print_heading('Detected status: ')
    assert capsys.readouterr().out == '\033[1;34m::\033[0m Detected status: '


def test_print_subheading(capsys):
    utils.print_subheading('Doing something... ')
    assert capsys.readouterr().out == '\033[1;33m=>\033[0m Doing something... '


def test_print_info(capsys):
    utils.print_info('Valid')
    assert capsys.readouterr().out == '\033[34mValid\033[0m\n'


def test_print_success(capsys):
    utils.print_success('Completed')
    assert capsys.readouterr().out == '\033[32mCompleted\033[0m\n'


def test_print_warning(capsys):
    utils.print_warning('Unsuccessful')
    assert capsys.readouterr().out == '\033[31mUnsuccessful\033[0m\n'
