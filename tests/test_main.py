"""
Test cases for the main module.

Author: Benedikt Vollmerhaus
License: MIT
"""

import logging
import pathlib
from unittest.mock import create_autospec

import pytest

from blurwal import __main__, paths
from blurwal.backends import base


def test_parse_args_exits_when_steps_below_2():
    with pytest.raises(SystemExit):
        __main__.parse_args(['-s', '1'])


def test_parse_args_verbose_sets_log_level():
    __main__.parse_args(['--verbose'])
    assert logging.getLogger().getEffectiveLevel() == logging.INFO


def test_parse_args_debug_sets_log_level():
    __main__.parse_args(['--debug'])
    assert logging.getLogger().getEffectiveLevel() == logging.DEBUG


def test_main_exits_when_no_backend_available(mocker):
    mocker.patch('blurwal.__main__.parse_args')
    mocker.patch('blurwal.environment.get_backend', return_value=None)
    with pytest.raises(SystemExit):
        __main__.main()


def test_main_restores_original_when_current_is_transition(mocker):
    mocker.patch('blurwal.__main__.Blur')
    mocker.patch('blurwal.__main__.parse_args')

    backend_mock = create_autospec(base.Backend)
    backend_mock.get_current.return_value = paths.CACHE_DIR / 'frame-4.jpg'
    mocker.patch('blurwal.environment.get_backend', return_value=backend_mock)

    __main__.main()
    backend_mock.restore_original.assert_called_once()


def test_main_sets_original_when_current_is_not_transition(mocker):
    mocker.patch('blurwal.__main__.Blur')
    mocker.patch('blurwal.__main__.parse_args')

    backend_mock = create_autospec(base.Backend)
    backend_mock.get_current.return_value = pathlib.Path('image.jpg')
    mocker.patch('blurwal.environment.get_backend', return_value=backend_mock)

    set_original_mock = mocker.patch('blurwal.paths.set_original')
    __main__.main()
    set_original_mock.assert_called_once_with(pathlib.Path('image.jpg'))
