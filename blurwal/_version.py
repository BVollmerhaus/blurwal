"""
The application's version for single-sourcing.

Author: Benedikt Vollmerhaus
License: MIT
"""

__version__ = '1.1.1'
